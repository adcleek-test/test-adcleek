import axios from 'axios'

export default() => {
  return axios.create({
    baseURL: process.env.VUE_APP_API_URL,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': true,
    }
  })
}